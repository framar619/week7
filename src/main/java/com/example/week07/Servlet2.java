package com.example.week07;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Servlet2", value = "/Servlet2")
public class Servlet2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><body>");
        String name = request.getParameter("name");
        String weight = request.getParameter("weight");
        String height = request.getParameter("height");
        out.println("<h1>BMI Calculator</h1>");
        out.println("<p>Name: " + name + "</p>");
        out.println("<p>Weight: " + weight + "</p>");
        out.println("<p>Height: " + height + "</p>");
        try {
            if (name == ""){
                throw new Exception("Exception message");

            }
        } catch (Exception e) {
            out.println("We need a non-empty value in the name" + "</p>");
        }
        try {
            if (isNumeric(name)){
                throw new Exception("Exception message");
            }
        } catch (Exception e) {
            out.println("We don't need a numeric value in the name"+ "</p>");
        }

        try {
            if (!isNumeric(weight)){
                throw new Exception("Exception message");
            }
        } catch (Exception e) {
            out.println("We need a numeric value in the weight" + "</p>");
        }

        try {
            if (!isNumeric(height)){
                throw new Exception("Exception message");
            }
        } catch (Exception e) {
            out.println("We need a numeric value in the height" + "</p>");
        }
        double result = Math.pow(Double.parseDouble(height), 2);
        out.println("<p>BMI: " + Double.parseDouble(weight) / result  + "</p>");
        out.println("</body></html>");
    }
    public static boolean isNumeric (String a){
        try {
            int d = Integer.parseInt(a);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;

    }
}
